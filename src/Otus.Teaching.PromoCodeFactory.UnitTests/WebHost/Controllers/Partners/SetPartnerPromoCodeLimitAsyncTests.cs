﻿using Microsoft.AspNetCore.Mvc;
using Moq;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System.Threading.Tasks;
using System;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using Xunit;
using FluentAssertions;
using Microsoft.EntityFrameworkCore.Query.Internal;
using System.Collections.Generic;
using System.Linq;
using AutoFixture.AutoMoq;
using AutoFixture;
using Castle.Core.Internal;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public class SetPartnerPromoCodeLimitAsyncTests
    {
        Guid testGuid;
        Partner testPartner;
        Partner savePartner;
        PartnersController testPartnersController;
        SetPartnerPromoCodeLimitRequest request;
        Mock<IRepository<Partner>> partnersRepository;


        public SetPartnerPromoCodeLimitAsyncTests()
        {
            Fixture fixture = new Fixture();
            request = fixture.Create<SetPartnerPromoCodeLimitRequest>();


            testGuid = Guid.NewGuid();

            partnersRepository = new Mock<IRepository<Partner>>();

            testPartner = new Partner();
            testPartner.Id = testGuid;

            partnersRepository.Setup(s => s.GetByIdAsync(It.IsAny<Guid>()))
                .Returns((Guid id) => {
                    if (id == testGuid)
                    {
                        return Task.FromResult(testPartner);
                    }
                    else
                    {
                        return Task.FromResult<Partner>(null);
                    }
                });

            partnersRepository.Setup(s => s.UpdateAsync(It.IsAny<Partner>()))
                .Callback((Partner p) => savePartner = p);


            testPartnersController = new PartnersController(partnersRepository.Object);


        }

        private void AddLimitToPartner(Partner p, int limit, DateTime endDate )
        {
            if (p.PartnerLimits.IsNullOrEmpty())
            {
                p.PartnerLimits = new List<PartnerPromoCodeLimit>();

            }

            var newLimit = new PartnerPromoCodeLimit()
            {
                Limit = limit,
                Partner = p,
                PartnerId = p.Id,
                CreateDate = DateTime.Now,
                EndDate = endDate
            };
            p.PartnerLimits.Add(newLimit);


        }




        // 1 Если партнер не найден, то нужно выдать ошибку 404;
        [Fact]
        public async void SetPartnerPromoCodeLimitAsyncTest_PartnerIsNotFound_ReturnsNotFound()
        {
            // Arrange

            // Act
            var res = await testPartnersController.SetPartnerPromoCodeLimitAsync(Guid.NewGuid(), request);

            // Asserts
            res.Should().BeAssignableTo<NotFoundResult>();
        }

        //2. Если партнер заблокирован, то есть поле IsActive=false в классе Partner, то также нужно выдать ошибку 400;
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PartnerIsActiveFalse_BadRequest()
        {
            // Arrange
            testPartner.IsActive = false;

            // Act
            var res = await testPartnersController.SetPartnerPromoCodeLimitAsync(testGuid, request);

            // Asserts
            res.Should().BeAssignableTo<BadRequestObjectResult>();
        }

        // 3. Если партнеру выставляется лимит, то мы должны обнулить количество промокодов,
        // которые партнер выдал NumberIssuedPromoCodes,
        // если лимит закончился, то количество не обнуляется;
        [Theory]
        [InlineData(0)]
        [InlineData(-1)]
        public async void SetPartnerPromoCodeLimitAsync_PartnerSetLimit_ResetLimits(int testDay)
        {
            // Arrange
            request.Limit = int.MaxValue;

            testPartner.IsActive = true;
            testPartner.NumberIssuedPromoCodes = int.MaxValue;

            AddLimitToPartner(testPartner, 0, DateTime.Now.AddDays(testDay));

            // Act
            await testPartnersController.SetPartnerPromoCodeLimitAsync(testGuid, request);

            // Asserts
            savePartner.NumberIssuedPromoCodes.Should().BeOneOf(0, int.MaxValue);
        }


        //4. При установке лимита нужно отключить предыдущий лимит;
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PartnerSetLimit_CancelOldLimits()
        {
            // Arrange
            request.Limit = int.MaxValue;

            testPartner.IsActive = true;
            testPartner.NumberIssuedPromoCodes = int.MaxValue;

            AddLimitToPartner(testPartner, 0, DateTime.Now);

            // Act
            await testPartnersController.SetPartnerPromoCodeLimitAsync(testGuid, request);
            PartnerPromoCodeLimit oldLimit = savePartner.PartnerLimits.FirstOrDefault(s => s.Limit == 0);

            // Asserts
            oldLimit.CancelDate.Should().NotBeNull();
        }

        //5. Лимит должен быть больше 0;
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PartnerSetZeroLimit_BadRequest()
        {
            // Arrange
            request.Limit = int.MinValue;

            testPartner.IsActive = true;

            // Act
            var res = await testPartnersController.SetPartnerPromoCodeLimitAsync(testGuid, request);

            // Asserts
            res.Should().BeAssignableTo<BadRequestObjectResult>();
        }

        //Нужно убедиться, что сохранили новый лимит в базу данных(это нужно проверить Unit-тестом);
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PartnerSetLimit_LimitSaved()
        {
            // Arrange
            request.Limit = int.MaxValue;

            testPartner.IsActive = true;

            AddLimitToPartner(testPartner, 0, DateTime.Now);

            // Act
            var res = await testPartnersController.SetPartnerPromoCodeLimitAsync(testGuid, request);
            PartnerPromoCodeLimit newLimit = savePartner.PartnerLimits.FirstOrDefault(s => s.Limit == int.MaxValue);

            // Asserts
            newLimit.Should().NotBeNull();

        }

    }
}